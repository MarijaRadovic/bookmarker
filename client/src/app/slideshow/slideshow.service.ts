import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SlideshowService {

  private readonly bookUrl = 'http://localhost:3000/book';
  private readonly reviewsUrl = 'http://localhost:3000/review';
  private readonly authorsUrl = 'http://localhost:3000/author';

  constructor(private http: HttpClient) { }

  public getBooks() : Observable<any>{
    return this.http.get<any>(this.bookUrl);
  }

  public getReviews() : Observable<any>{
    return this.http.get<any>(this.reviewsUrl);
  }

  public getAuthors() : Observable<any>{
    return this.http.get<any>(this.authorsUrl);
  }
}
