declare var require: any
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { JwtService } from '../common/services/jwt.service';
import { User } from '../models/user.model';
import { AuthService } from './auth.service';
var FormData = require('form-data');


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly userUrl = 'http://localhost:3000/users/';
  private readonly patchUserImageUrl = this.userUrl + 'profileImage';

  constructor(private http: HttpClient,
    private jwtService: JwtService,
    private authService: AuthService) { }


  getUserInfo(idUser: any): Observable<User> {
    return this.http.get<User>(this.userUrl + idUser);
  }

  follow(idUser: string, idFollow: string): Observable<User> {
    const body = { idUser, idFollow };
    return this.http.patch<{ token: string }>(this.userUrl + 'follow', body).pipe(
      tap((response: { token: string }) => this.jwtService.setToken(response.token)),
      map((response: { token: string }) => this.authService.sendUserDataIfExists())
    );
  }

  unfollow(idUser: string, idUnfollow: string): Observable<User> {
    const body = { idUser, idUnfollow };
    return this.http.patch<{ token: string }>(this.userUrl + 'unfollow', body).pipe(
      tap((response: { token: string }) => this.jwtService.setToken(response.token)),
      map((response: { token: string }) => this.authService.sendUserDataIfExists())
    );
  }

  patchUserData(data: any): Observable<User> {
    const body = {...data};
    return this.http.patch<{ token: string }>(this.userUrl, body).pipe(
      tap((response: { token: string }) => this.jwtService.setToken(response.token)),
      map((response: { token: string }) => this.authService.sendUserDataIfExists())
    );
  }

  public patchUserProfileImage(file: File, username: string): Observable<User> {
    const img: FormData = new FormData();
    img.append("image", file);
    img.append("username", username);

    return this.http.patch<{ token: string }>(this.userUrl + 'profile-image', img).pipe(
      tap((response: { token: string }) => this.jwtService.setToken(response.token)),
      map((response: { token: string }) => this.authService.sendUserDataIfExists())
    );
  }

  public createImage(data: any): Observable<any> {
    const body = { ...data };
    return this.http.post<any>(this.userUrl, body);
  }
}
