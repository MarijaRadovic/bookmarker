import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
import { JwtService } from '../common/services/jwt.service';
import { IJWTTokenData } from '../common/services/models/jwt-token-data';
import { User } from '../models/user.model';
import { throwError } from 'rxjs';


export interface IMsg {
  user: User;
  success: boolean;
}

export class Msg implements IMsg {
  user: User;
  success: boolean;
  constructor(msgUser: any, msgSuccess: any) {
    this.user = msgUser;
    this.success = msgSuccess;
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly userSubject: Subject<User> = new Subject<User>();
  public readonly user: Observable<User> = this.userSubject.asObservable();

  private readonly url = {
    registerUser: 'http://localhost:3000/auth/register',
    loginUser: 'http://localhost:3000/auth/login'
  }

  constructor(private http: HttpClient,
              private jwtService: JwtService) { }


  public sendUserDataIfExists(): User {
    const payload: IJWTTokenData = this.jwtService.getDataFromToken();
        if (!payload){
          return null;
        }

        const user: User = new User(payload.id, payload.name, payload.surname, payload.email, payload.username, payload.image, payload.following, payload.followers, payload.age, payload.place, payload.role);
        this.userSubject.next(user);
        return user;
  }

  public loginUser(data: any): Observable<User>{
    const body = {...data};
     return this.http.post<{token: string}>(this.url.loginUser, body).pipe(
       tap((response: {token: string}) => this.jwtService.setToken(response.token)),
       map((response: {token: string}) => this.sendUserDataIfExists()),
       catchError(this.errorHandler)
    );
  }

  errorHandler(error: HttpErrorResponse) {
      return throwError(error || 'Server error');
  }


  public logoutUser() {
    this.jwtService.removeToken();
    this.userSubject.next(null);
  }


  public getAllUsers(){
    return this.http.get(this.url.registerUser);
  }

  public createUser(data: any): Observable<User>{
    const body = {...data};
    return this.http.post<{token: string}>(this.url.registerUser,body).pipe(
      // tap((response: {token: string}) => this.jwtService.setToken(response.token)),
      map((response: {token: string}) => this.sendUserDataIfExists())
    );
  }
  
  
}
