import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { JwtService } from '../common/services/jwt.service';
import { IJWTTokenData } from '../common/services/models/jwt-token-data';
import { User } from '../models/user.model';
import { RegisterComponent } from './register.component';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  public registerComponent: RegisterComponent;
  public users: User[] = [];

  private readonly usersUrl = 'http://localhost:3000/auth/register';

  constructor(private http: HttpClient,
              private jwtService: JwtService) { }

  


}
