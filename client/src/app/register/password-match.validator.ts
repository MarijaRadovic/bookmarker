import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function passwordMatch(): ValidatorFn{
    return (control: AbstractControl): ValidationErrors | null=> {
        const password1 = control.root.get('password');
        const password2 = control.root.get('confirmPassword')
        if(password2){
            const match = password1.value === password2.value;
                return match ? null : {differentPasswords: true};
            }
        else
            return null;
    }
}