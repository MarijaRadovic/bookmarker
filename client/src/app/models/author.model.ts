export interface Author {
    name:String,
    bio:String,
    imagePath: String,
    nationality: String,
    born: Number,
    died: Number,
    books:Array<any>
}