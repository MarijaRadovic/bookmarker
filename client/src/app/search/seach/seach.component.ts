import { AfterViewInit, Component, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Author } from 'src/app/models/author.model';
import { Book } from 'src/app/models/book.model';
import { BookReview } from 'src/app/models/review.model';
import { AuthorSearchI, BookSearchI, getAuthorSearchStrings, getBookSearchStrings, getReviewSearchStrings, getSearchType, getSearchWrapperNameFor, getStorySearchStrings, getUserSearchStrings, ReviewSearchI, SearchType, StorySearchI, UserSearchI } from 'src/app/models/search.model';
import { Story } from 'src/app/models/story.model';
import { UserI } from 'src/app/models/user.model';
import { findPath, isEmpty } from 'src/app/util/utils';
import { ChipData } from '../bookm-chip/bookm-chip.component';
import { FilterField } from '../filter-field/filter-field.component';
import { ResultInfo } from '../result-card/result-card.component';
import { SearchService } from './search.service';

import {FormSelect} from 'materialize-css';

@Component({
  selector: 'app-search',
  templateUrl: './seach.component.html',
  styleUrls: ['./seach.component.css']
})
export class SearchComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  private searchSubscription : Subscription;
  options = {};
  
  searchBy: SearchType;
  readonly searchBySelectID = "bm-s-select-searchBy"
  searchByList: SearchType[] = [SearchType.USER, SearchType.STORY , SearchType.BOOK ,  SearchType.AUTHOR , SearchType.REVIEW];
  selectInstance : FormSelect;

  fields: FilterField[];

  filterValues: StorySearchI | UserSearchI | BookSearchI | AuthorSearchI | ReviewSearchI;
  chips : ChipData[] = [];

  filterResults: ResultInfo[] = [];


  constructor(private searchService: SearchService) { 

    // this.restoreComponentState();
  }

  ngOnInit(): void {
    // console.log(M);
    // M.FormSelect.init([document.getElementById(this.searchBySelectID)])
    
    // window.onbeforeunload = () => this.ngOnDestroy(); // ovo je ako treba da se obrise pre refresha
  }
  
  ngAfterViewInit(){
    FormSelect.init(document.querySelector('select#'+this.searchBySelectID));
    this.selectInstance = FormSelect.getInstance(document.getElementById(this.searchBySelectID));
  }

  ngOnChanges():void {
    this.storeComponentState();
  }

  isUndefinedSearch(){
    return this.searchBy === undefined;
  }

  isSelected(value: string) : boolean{    
    if(value === this.searchBy){      
      return true;
    }
    return false;
  }

  onSearchByChange(value: string){
    this.filterValues = {};
    if(value === SearchType.USER){
      this.fields = getUserSearchStrings();
      this.searchBy = SearchType.USER;
    }
    else if(value === SearchType.AUTHOR){
      this.fields = getAuthorSearchStrings();
      this.searchBy = SearchType.AUTHOR;
    }
    else if(value === SearchType.BOOK){
      this.fields = getBookSearchStrings();
      this.searchBy = SearchType.BOOK;
    }
    else if(value === SearchType.STORY){
      this.fields = getStorySearchStrings();
      this.searchBy = SearchType.STORY;
    }
    else if(value === SearchType.REVIEW){
      this.fields = getReviewSearchStrings();
      this.searchBy = SearchType.REVIEW;
    }
    // this.storeComponentState();
  }

  onFieldsChange(fields: FilterField[]){
    this.fields = fields;
    fields.forEach((field: FilterField)=>{
      if(field.fieldName === "keywords"){
        this.filterValues = {...this.filterValues, [field.fieldName]: field.currentValue?.split(new RegExp('[ ,.]'))};
      }
      else{
        this.filterValues = {...this.filterValues, [field.fieldName]: field.currentValue};
      }
    });
    // this.storeComponentState();
  }

  onRunSearch() : void {
    this.chips = this.fields.filter((value:FilterField)=> !isEmpty(value.currentValue)).map((value:FilterField)=>{
      return {
        id: value.fieldName,
        tag: value.currentValue,
      } as ChipData;
    });
    // todo poziv servisa testrati sve
    const searchByName = getSearchWrapperNameFor(this.searchBy);
    if(this.searchSubscription !== undefined){
      this.searchSubscription.unsubscribe();
    }
    this.searchSubscription = this.searchService.runSearch({[searchByName] : this.filterValues})
      .subscribe(
        (data: Array<Object>)=> {
          if(searchByName === "author"){ 
            const author = data as Author[];
            this.filterResults = author.map((a: Author)=> ({
              image: findPath(a.imagePath), // findPath (author.imagePath)
              description: a.bio,
              route: ['/author'], // TODO: kada se autor zavrsi dopuniti rutu sa id ili kombinacijom ime-prezime
              title: a.name
            } as ResultInfo));
          }
          if(searchByName === "user"){ 
            const user = data as UserI[];
            this.filterResults = user.map((a: UserI)=> ({
              image: a.image,
              description: `${a.name} ${a.surname}`,
              route: ['/dashboard', a._id],
              title: a.username
            } as ResultInfo));
          }
          if(searchByName === "book"){ 
            const book = data as Book[];
            this.filterResults = book.map((a: Book)=> ({
              image: findPath(a.coverImagePath),
              description: `${a.genre.join(', ')} - ${a.description}`,
              route: ['/book', a._id],
              title: a.title
            } as ResultInfo));
          }
          if(searchByName === "story"){ 
            const author = data as Story[];
            this.filterResults = author.map((a: Story)=> ({
              description: a.body,
              // route: ['author'], 
              title: a.title
            } as ResultInfo));
          }
          if(searchByName === "review"){ 
            const author = data as BookReview[];
            this.filterResults = author.map((a: BookReview)=> ({
              description: a.reviews.comment,
              // route: ['author'], // TODO: videti kako ovo ide
              title: a.reviews.title
            } as ResultInfo));
          }
        }
      );
    // this.storeComponentState();
  }

  private restoreComponentState(): void{
    if(localStorage.getItem("filterFields") != null){
      this.fields = JSON.parse(localStorage.getItem("filterFields"));
    }
    if(localStorage.getItem("searchBy") != null){
      this.searchBy = getSearchType(JSON.parse(localStorage.getItem("searchBy")));
    }
    if(localStorage.getItem("filterValues") != null){
      this.filterValues = JSON.parse(localStorage.getItem("filterValues"));
    }
    console.log("refresh, vracanje stanje",
      this.fields,
      this.searchBy,
      this.filterValues
    );
  }

  private storeComponentState(): void{
    console.log("doslo do promene, pamti stanje",
      JSON.stringify(this.fields),
      JSON.stringify(this.searchBy),
      JSON.stringify(this.filterValues)
    );
    localStorage.setItem("filterFields", JSON.stringify(this.fields));
    localStorage.setItem("searchBy", JSON.stringify(this.searchBy));
    localStorage.setItem("filterValues", JSON.stringify(this.filterValues));
  }

  ngOnDestroy(){
    if(this.searchSubscription){
      this.searchSubscription.unsubscribe();
    }
    localStorage.removeItem("filterFields");
    localStorage.removeItem("searchBy");
    localStorage.removeItem("filterValues");
  }

}
