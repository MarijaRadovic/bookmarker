import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookmChipComponent } from './bookm-chip.component';

describe('BookmChipComponent', () => {
  let component: BookmChipComponent;
  let fixture: ComponentFixture<BookmChipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookmChipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmChipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
