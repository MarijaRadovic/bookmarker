import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

export interface ChipData{
  id: string,
  tag: string,
  image?: string,
  imageAlt?: string,
}

@Component({
  selector: 'app-bookm-chip',
  templateUrl: './bookm-chip.component.html',
  styleUrls: ['./bookm-chip.component.css']
})
export class BookmChipComponent implements OnInit {

  @Input() chipData : ChipData;
  @Output() onChipDeleted = new EventEmitter<ChipData>();

  constructor() { }

  ngOnInit(): void {
    console.log(this.chipData);
  }

}
