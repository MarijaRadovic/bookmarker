import { Component, Input, OnInit } from '@angular/core';

export interface ResultInfo{
  image?: string,
  title?: string,
  description?: string,
  link?: string,
  route?: string[]
}

@Component({
  selector: 'app-result-card',
  templateUrl: './result-card.component.html',
  styleUrls: ['./result-card.component.css']
})
export class ResultCardComponent implements OnInit {

  @Input() resultInfo: ResultInfo = {};

  constructor() {
  }

  ngOnInit(): void {
    
  }

}
