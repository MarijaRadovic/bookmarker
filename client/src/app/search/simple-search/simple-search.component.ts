import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Author } from 'src/app/models/author.model';
import { Book } from 'src/app/models/book.model';
import { BookReview } from 'src/app/models/review.model';
import { Story } from 'src/app/models/story.model';
import { UserI } from 'src/app/models/user.model';
import { findPath, isEmpty } from 'src/app/util/utils';
import { ResultInfo } from '../result-card/result-card.component';
import { SimpleSearchService } from './simple-search.service';

@Component({
  selector: 'app-simple-search',
  templateUrl: './simple-search.component.html',
  styleUrls: ['./simple-search.component.css']
})
export class SimpleSearchComponent implements OnInit, OnDestroy {
  private searchSubscription : Subscription;
  searchInputVisible: boolean = false;
  popupStyle: {} = {
    display: 'none'
  };
  searchFieldInFocus: boolean = false;
  advancedVisible: boolean = true;
  
  visibleProgress: boolean = false;
  progressClasses: string[] = ['progress', 'invisible'];

  results: Array<{header: string, results: ResultInfo[]}> = [];

  fieldValue : string = "";
  hidePopup: boolean = false;

  hideHeaderSearch: boolean = false;

  isSimpleSearch: boolean = true;

  routerListener: Subscription;

  constructor(
    private service: SimpleSearchService,
    private router: Router,
    private location: Location
  ) { 
    this.routerListener = this.router.events.subscribe((val)=>{      
      if(this.location.path().endsWith('search')){
        this.hideHeaderSearch=true;
      }
      else{
        this.hideHeaderSearch=false;
      }
    })
  }

  toggleSimpleSearch(){
    if(this.isSimpleSearch && !isEmpty(this.fieldValue)){
      this.fieldValue = "";
      this.results = [];
    }
    this.isSimpleSearch = !this.isSimpleSearch;
    this.setPopupVisibility(!this.isSimpleSearch);
    this.hidePopup = false;
  }

  ngOnInit(): void {
  }
  
  onFieldChange(event: Event){
    this.fieldValue = (event.target as HTMLInputElement).value;
  }

  onSearchFieldBlur(){
    if(isEmpty(this.fieldValue)){
      this.searchInputVisible = false;
    }
  }

  onMouseOverSearchField(isOver: boolean){
    // if(!this.isSimpleSearch){
    //   this.isSimpleSearch = true;
    //   this.setPopupVisibility(true);
    //   this.hidePopup=true;
    // }
    // else{
    //   this.onSearchFieldFocus(true);
    // }
    
    if(this.isSimpleSearch){
      setTimeout(() => {
        this.onSearchFieldFocus(isOver);
      }, 1500);
    }
  }

  onSearchFieldFocus(isFocused: boolean){
    
    if(!isFocused && isEmpty(this.fieldValue)){
      this.searchInputVisible = false;
    }
    if(isFocused && !this.searchFieldInFocus){
      this.searchFieldInFocus = true;
    }
    
    // if(!this.searchInputVisible){
    //   this.searchInputVisible=true;
    // }

    this.hidePopup = !isFocused;
 
  }

  onSimpleSearchHover(isMouseOver: boolean){
    if(this.isSimpleSearch){
      if(isMouseOver){
        this.searchInputVisible = true;
      }
      if(!isMouseOver){
        setTimeout(()=>{
          if(!this.searchFieldInFocus){
            this.searchInputVisible = isMouseOver
          }
        }, 1500)
      }
    }
  }

  onInputKeypress(event: KeyboardEvent){
    if(event.key==="Enter"){
      this.onRunSearch();
    }
  }

  onRunSearch(){
    
    if (!isEmpty(this.fieldValue)) {
      this.isSimpleSearch = true;
      this.visibleProgress = true;
      this.progressClasses = [...this.progressClasses.filter(className=> className!=="invisible"), "visible"]
      
      if (this.searchSubscription !== undefined) {
        this.searchSubscription.unsubscribe();
      }
      try {
        this.searchSubscription = this.service.runSearch(this.fieldValue.trim()).subscribe((data: Array<object>) => {
          this.results = data
            .filter((nonEmpty: object) => {
              return Object.values(nonEmpty)[0].length != 0;
            })
            .map((section: object) => {              
              if (Object.keys(section)[0] === "author") {
                return {
                  header: "Authors",
                  results: Object.values(section)[0].map((author: Author) => ({
                    title: author.name,
                    description: author.bio,
                    image: findPath(author.imagePath),
                    route: ['/author'],
                  } as ResultInfo))
                }
              }
              if (Object.keys(section)[0] === "user") {
                return {
                  header: "Users",
                  results: Object.values(section)[0].map((user: UserI) => ({
                    title: `${user.name} ${user.surname}`,
                    description: user.username,
                    image: user.image,
                    route: ['/dashboard', user._id],
                  } as ResultInfo))
                }
              }
              if (Object.keys(section)[0] === "book") {
                return {
                  header: "Books",
                  results: Object.values(section)[0].map((book: Book) => ({
                    title: book.title,
                    description: book.description,
                    image: findPath(book.coverImagePath),
                    route: ['/book', book._id],
                  } as ResultInfo))
                }
              }
              if (Object.keys(section)[0] === "story") {
                return {
                  header: "Stories",
                  results: Object.values(section)[0].map((story: Story) => ({
                    title: story.title,
                    description: `${story.body}`,//@${story.user} - ovo je isto id usera, servis da vrati i username
                    route: ['/stories', story.user],
                  } as ResultInfo))
                }
              }
              if (Object.keys(section)[0] === "review") {
                return {
                  header: "Reviews",
                  results: Object.values(section)[0].map((review: BookReview) => {
                    return ({
                      title: `${review.reviews?.title}`,// - @${review.reviews?.user}`, ovo je id usera, ne njegovo ime. treba dodati da se kroz servis vrati i username pored id-a
                      description: review.reviews?.comment,
                      // image: review.image,
                      route: ['/book', review.book],
                    } as ResultInfo)
                  })
                }
              }
              return { header: "", results: [] };
            });
          this.setPopupVisibility(true);
          this.visibleProgress = false;
          this.progressClasses = [...this.progressClasses.filter(className => className !== "visible"), "invisible"];
        })
      } catch (err) {
        console.log(err);
        this.visibleProgress = false;
        this.progressClasses = [...this.progressClasses.filter(className => className !== "visible"), "invisible"];

      }
    }
  }

  onClickOutsidePopup(){
    setTimeout(()=>{
      if(this.hidePopup){
        this.setPopupVisibility(false);
        this.results = [];
        this.fieldValue = "";
      }
    }, 500);
  }

  setPopupVisibility(visible: boolean){
    
    this.popupStyle = {
      ...this.popupStyle,
      display: visible? 'unset' : 'none'
    }
    this.hidePopup=visible;
  }

  onClosePopup(){
    this.hidePopup=true;
    this.onClickOutsidePopup();
  }

  ngOnDestroy(){
    if(this.searchSubscription){
      this.searchSubscription.unsubscribe();
    }
    if(this.routerListener){
      this.routerListener.unsubscribe();
    }
  }
}