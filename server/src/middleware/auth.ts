import { StatusCodes } from "http-status-codes";
import { Request, Response } from "express";
import { UserRole } from "../models/user";
const jwt = require('./jwt');

export function ensureAdmin (req: Request, res:Response, next: Function) {
    console.log("usao u ensure");
    
    const authHeader = req.header("Authorization");
    if (!authHeader) {
        const error = new Error('You need to pass Authorization header with your request!');
        // error.status = 403;
        throw error;
    }
    const token = authHeader.split(' ')[1];
    const decodedToken = jwt.verifyJWT(token);
    if (!decodedToken) {
        const error = new Error('Not Authenticated!');
        // error.status = 401;
        throw error;
    }
    let role = decodedToken.role;
    console.log('decoded role:', role);
    if(role && (role as UserRole === "admin")){
        next();
    }
    else{
        res.status(StatusCodes.UNAUTHORIZED).send("No content.");
    }
  
}

export function ensureAuth (req: Request, res:Response, next: Function) {
    if(req.isAuthenticated()){
        return next();
    }
    else{
        // res.redirect('/')
        res.status(StatusCodes.UNAUTHORIZED).send();
    }
}
export function ensureGuest(req: Request, res: Response, next: Function){
    if(req.isAuthenticated()){
        // res.redirect('/dashboard')
        res.status(StatusCodes.OK).send();
    }
    else{
        return next();
    }
}