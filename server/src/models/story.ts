import mongoose, { Model, Document } from 'mongoose';
import { ObjectId } from 'mongodb';

export interface StorySearchI {
    title?: string;
    fromDate?: string;
    toDate?: string;
    keywords?: string[];
}

export interface IStoryDocument extends Document {
    title: string,
    body: string,
    user: mongoose.Schema.Types.ObjectId,
    createdAt: Date,
    usersThatLikeStory: [ObjectId]
}

export interface IStoryModel extends Model<IStoryDocument> {
    // here we decalre statics
    addStory(storyTitle, storyContent, storyAuthor): any
    findForUser(userId): any
    getUsersThatLiked(storyId): any
    like(storyId, userId): any
    dislike(storyId, userId): any
}

const storySchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        trim: true
    },
    body: {
        type: String,
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now
    },

    usersThatLikeStory: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User', default: []
    }]
});

storySchema.statics.addStory = async function (storyTitle, storyContent, storyAuthor) {
    const story = new this({
        'title': storyTitle,
        'body': storyContent,
        'user': storyAuthor
    });
    const savedStory = await story.save();
    return savedStory;
};

storySchema.statics.findForUser = async function (userId) {
    const story = await this.find({ user: userId }).populate('user').sort({ createdAt: 'desc' }).lean()
    return story;
}

storySchema.statics.getUsersThatLiked = async function (storyId) {
    const story = await this.findById(storyId);
    console.log(story.usersThatLikeStory)
    return story.usersThatLikeStory;
}

storySchema.statics.like = async function (storyId, userId) {
    const story = await this.findById(storyId);
    story.usersThatLikeStory.push(userId);
    await story.save();

    return story ? Promise.resolve(story) : Promise.reject();
}

storySchema.statics.dislike = async function (storyId, userId) {
    const story = await this.findById(storyId);

    story.usersThatLikeStory.forEach((value,index)=>{
        if(value==userId) 
            story.usersThatLikeStory.splice(index,1);
    });

    await story.save();

    return story ? Promise.resolve(story) : Promise.reject();
}

const Story = mongoose.model<IStoryDocument, IStoryModel>('Story', storySchema);


export async function searchStories(options:StorySearchI) {
       
    let result : Array<object> = [];
    let criteriaOptions = {};
    if(options.title){
        criteriaOptions = {...criteriaOptions, "title": {$regex: options.title, $options: "i"}};
    }
    if(options.keywords && options.keywords.length !== 0){
        criteriaOptions = {...criteriaOptions, $text: {$search: options.keywords.join(' ')}}
    }
    let dateTo;
    let dateFrom;
    if(options.fromDate){ // format 2015-07-07T00:00:00.000Z
        dateFrom = {"createdAt": {$gte : new Date(options.fromDate)}}
    }
    if(options.toDate){
        dateTo = {"createdAt": {$lte : new Date(options.toDate)}}
    }
    if(options.fromDate && options.toDate){ // postoje oba
        criteriaOptions = {...criteriaOptions, $and: [dateFrom, dateTo]};
    }
    else if(options.fromDate){
        criteriaOptions = {...criteriaOptions, dateFrom};
    }
    else if(options.toDate){
        criteriaOptions = {...criteriaOptions, dateTo};
    }
    (await Story.find(criteriaOptions).exec()).forEach(story=>{
        result.push(story);
    });
    return result;
}

export async function simpleSearchStory(keywords: string){
    let result: Array<object> = [];
    const indexName = "s_ss_title_body";
    let indexes: object = await Story.collection.getIndexes();
    if(!Object.keys(indexes).includes(indexName)){
        await Story.collection.createIndex({
            title: "text",
            body: "text",
        }, {
            name: indexName
        });
    }

    (await Story.find({$text: {$search: keywords}})).forEach(story=>{
        result.push(story);
    });
    return result;
}
export default Story;