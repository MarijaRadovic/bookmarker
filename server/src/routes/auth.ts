import { StatusCodes } from "http-status-codes";

const express = require('express');
const passport = require('passport');
const authentication = require('../middleware/authentication');
import User from '../models/user';

const authRouter = express.Router();

//@desc     Auth with Google
//@route    GET/auth/google
authRouter.get('/google', passport.authenticate('google', { scope: ['profile'] }))

//@desc     Google auth callback
//@route    GET/auth/google/callback
authRouter.get('/google/callback', passport.authenticate('google', { failureRedirect: '/' }), 
    (req, res) => {
        res.sendStatus(StatusCodes.OK);
        // res.redirect('/dashboard')
    }
)

//@desc     Logout user
//@route    /auth/logout
authRouter.get('/logout', (req, res) => {
    req.logout()
    res.redirect('/')
})

authRouter.get('/register', async (req, res) => {
    try {
        const users = await User.find();
        res.json(users);
    }
    catch (err) {
        console.error("[err while geting all stories]", err);
    }
});

authRouter.post('/register', async (req, res, next) => {
    const name = req.body.name;
    const surname = req.body.surname;
    const email = req.body.email;
    const username = req.body.username;
    const password = req.body.password;
    try {
        const jwt = await User.registerNewUser(name, surname, email, username, password);
            return res.status(201).json({
                token: jwt,
            });
    } catch (err) {
        next(err);
      }

});

// authRouter.post('/login',  async (req, res, next) => {

//     const username = req.body.username;
//     const password = req.body.password;
//     const secret = 'BastlewandonLahmPhillipterernAlliaianSchwethomasmugkenzArenadesOellerinsteigerSehenshnSudewuerdiwnHigerStlskiteiverKans';

//     try {
//         const user = await User.getUserByUsername(username);

//         if (user) {
//             if (await user.comparePassword(password)) {
        
//                 req.session.user = username;
//                 req.session.save();

//                 console.log("saved cookie");
//                 console.log(req.session);

//                 res.json({ 'success': true, 'message': 'User is successfully logged!' });
//             } else {
//                 res.json({ 'success': false, 'message': 'Password is not correct!' });
//             }
//         } else {
//             res.json({ 'success': false, 'message': "Password or username is not correct" });
//         }
//     } catch (err) {
//         next(err);
//     }
// });

authRouter.post('/login', authentication.canAuthenticate, async (req, res, next) => {

    const username = req.username;
    
    try {
      const jwt = await User.getUserJWTByUsername(username);
      return res.status(201).json({
        token: jwt
      });
    } catch (err) {
      next(err);
    }
});

module.exports = authRouter;